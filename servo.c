#include<mraa.h>
#include<time.h>



int
main(int argc, char** argv)
{
    if(argc!=4){
        printf("Usage: servo <pin_no> <pulsewidth in us> <send for time in ms>\n");
        return -1;
    }
    
    int iopin=atoi(argv[1]);
    unsigned int pulseLen=atoi(argv[2]);
    unsigned int totalTime=atoi(argv[3]);
    
    mraa_result_t r = MRAA_SUCCESS;
    mraa_init();
   // fprintf(stdout, "MRAA Version: %s\nStarting Blinking on IO%d\n", mraa_get_version(), iopin);

    mraa_pwm_context pwm;
    pwm = mraa_pwm_init(iopin);
    if (pwm == NULL) {
        fprintf(stderr, "Are you sure that pin%d you requested is valid on your platform?", iopin);
        return 1;
    }
    printf("Initialised pin%d\n", iopin);


    struct sched_param param;
    param.sched_priority=99;
    sched_setscheduler(getpid(),SCHED_FIFO,&param); //Set realtime priority!
    
    mraa_pwm_period_ms(pwm, 20);
    mraa_pwm_pulsewidth_us(pwm,pulseLen);
    mraa_pwm_enable(pwm, 1);
    usleep(totalTime*1000);
            
    r = mraa_pwm_close(pwm);
    if (r != MRAA_SUCCESS) {
        mraa_result_print(r);
    }

    return r;
}